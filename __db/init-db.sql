####### << PROJEKT ARBEIT PASSWORD TRESOR >> #######


# >> DB PASSWORD_TRESOR
drop database password_tresor;
create database password_tresor;
use password_tresor;


# >> DB MYSQL USER -> CRUD permissions on all tables from db password_tresor!
-- drop user tresor_app_user@localhost;
create user tresor_app_user@localhost Identified by '1234';
GRANT SELECT, INSERT, DELETE, UPDATE on password_tresor.* TO tresor_app_user@localhost;
select * from mysql.user; -- check user

SHOW GRANTS FOR tresor_app_user@localhost;

GRANT UPDATE ON password_tresor.* TO tresor_app_user@localhost;
# >> TABLES:


# PASSWORD ARCHIVE ---------------------------------------------------------------------------------
-- drop table password_archive;
create table password_archive 
(
  id INT PRIMARY KEY AUTO_INCREMENT, 
  title VARCHAR(200) NOT NULL UNIQUE, 
  aes_crypting_password VARCHAR(128) NOT NULL # 128 Zeichen = 64Byte für sha512, 64Byte=512Bit
);

INSERT INTO password_archive(title, aes_crypting_password) 
VALUES("privat", "d404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db"),
("abteilung A", "d404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db"),
("Abteilung B", "d404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db");
select * from password_archive;
-- select * from password_archive where aes_crypting_password=PASSWORD("1234"); -- check authentification query --> result must be => ["privat", 1234]
-- desc password_archive;


# CATEGORY         ---------------------------------------------------------------------------------
-- drop table category;
create table category 
(
  id INT PRIMARY KEY AUTO_INCREMENT, 
  title varchar(200) NOT NULL, 
  parent_id INT, 
  archive_id INT, 
  CONSTRAINT constraint_archive_id_to_password_archive_id
    -- PRIMARY KEY (id)
	FOREIGN KEY (archive_id)
	REFERENCES password_archive (id) 
	ON DELETE CASCADE 
	ON UPDATE CASCADE, 
  CONSTRAINT constraint_parent_id_to_id_self_reference
    FOREIGN KEY (parent_id)
	REFERENCES category (id)
    ON DELETE CASCADE
	ON UPDATE RESTRICT -- CASCADE is either way not possible. Therefore RESTRICT
);

# mit ALTER TABLE
# ALTER TABLE category
#	ADD CONSTRAINT const_parent_id_to_id
#	foreign key (parent_id)
#	references category (id);

INSERT INTO category(title, parent_id, archive_id ) 
  values("categories", null, 1)
  ,("private", 1, 1) 
  ,("emails", 2, 1) 
  ,("gmail accounts", 3, 1) 
  ,("work", 1, 1);

INSERT INTO category(title, parent_id, archive_id ) values("categories-2", null, 1);

select * from category;

select @pv:=id as id, title, parent_id from category
join
(select @pv:=1)tmp
where parent_id=@pv;

-- desc category;

-- UPDATE password_archive set id=1 where id=51; -- check on update cascade
-- DELETE FROM category where id=2; -- check on delete cascade
-- DELETE FROM password_archive where id=1; -- check on delete cascade


# PASSWORD_DATA    ---------------------------------------------------------------------------------
-- drop table password_data;
create table password_data
(
  id int primary key auto_increment,
  title varchar(200) not null, 
  username varchar(200) not null,
  aes_crypted_password varchar(200) not null,
  category_id int,  
  constraint constraint_id_category_id
	foreign key (category_id)
	references category (id)
	ON DELETE CASCADE
	ON UPDATE RESTRICT -- CASCADE is either way not possible. Therefore RESTRICT
);

INSERT INTO password_data(title, username, aes_crypted_password, category_id) 
  VALUES("account 555555", "myUserName2510552@dfdsfdsf.com", "pw1", 2)
  ,("Gmail --> account 555551", "myUserName2510552@gmail.com", "1234", 4)
  ,("Gmail --> account 5552 !!", "myName@gmail.com", "5555", 4);

select * from password_data;
delete from password_data;
use password_tresor;
-- DELETE FROM category where id=4; test on delete cascade