package app.injector;

import app.dao.dao_interfaces.IArchiveDao;
import app.dao.dao_interfaces.ICategoryDao;
import app.dao.dao_interfaces.IPasswordDataDao;
import app.dao.mysql.MySqlArchiveDao;
import app.dao.mysql.MySqlCategoryDao;
import app.dao.mysql.MySqlPasswordDataDao;
import app.model.app_Settings.AppSettings;
import com.google.common.eventbus.EventBus;
import com.google.inject.AbstractModule;

/**
 * Created by Antic on 27.07.2016.
 */
public class InjectorConfiguration extends AbstractModule
{
  protected void configure() {
    bind(IArchiveDao.class).to(MySqlArchiveDao.class);
    bind(ICategoryDao.class).to(MySqlCategoryDao.class);
    bind(IPasswordDataDao.class).to(MySqlPasswordDataDao.class);
//    bind(InterPersonDao.class).to(InMemoryDB.class);
//    bind(InterPersonDao.class).to(MySqlPersonDao.class);
//    bind(InterPersonDao.class).to(JsonPersonDao.class);
//    bind(InterPersonDao.class).to(SQlitePersonDao.class);

    bind(AppSettings.class).asEagerSingleton();
    bind(EventBus.class).asEagerSingleton();
  }
}
