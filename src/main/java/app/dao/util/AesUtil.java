package app.dao.util;

import com.google.common.hash.Hashing;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.security.*;
import java.util.Arrays;

/**
 * Created by Antic on 18.08.2016.
 */
public class AesUtil
{
  private static final String algorythmus = "AES/ECB/PKCS5Padding";

  private static SecretKeySpec getKey(String password) {
    // without security policy is only a 128 bit long block shiffre possible
    try {
      byte[] bytePassword = password.getBytes("UTF-8");
      MessageDigest sha = MessageDigest.getInstance("SHA-256");
      byte[] key = Arrays.copyOf(sha.digest(bytePassword), 16);

      return new SecretKeySpec(key, "AES");
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }

    return null;
  }

  public static String encryptText(String password, String plainText) {
    try {
      Cipher cipher = Cipher.getInstance(algorythmus);
      cipher.init(Cipher.ENCRYPT_MODE, getKey(password));
      byte[] encrypted = cipher.doFinal(plainText.getBytes());

      Encoder encoder = Base64.getEncoder();
      byte[] encoded = encoder.encode(encrypted);
      return new String(encoded);
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }

    return null;
  }

  public static String decryptText(String password, String encryptedText) {
    try {
      Decoder decoder = Base64.getDecoder();
      byte[] decodedEncryptedText = decoder.decode(encryptedText);

      Cipher cipher = Cipher.getInstance(algorythmus);
      cipher.init(Cipher.DECRYPT_MODE, getKey(password));
      byte[] decryptedData = cipher.doFinal(decodedEncryptedText);
      return new String(decryptedData);
    } catch (Exception e) {
      System.err.println(e.getMessage());
      return null;
    }
  }

}
