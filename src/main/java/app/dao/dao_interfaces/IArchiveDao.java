package app.dao.dao_interfaces;

import app.model.Archive;
import javafx.collections.ObservableList;

/**
 * Created by Antic on 15.08.2016.
 */
public interface IArchiveDao
{
  public ObservableList<String> getAllArchiveTitles();

  public Archive checkLoginData(String archiveName, String password);

  public Archive getArchiveByArchiveId(int archiveId);
}
