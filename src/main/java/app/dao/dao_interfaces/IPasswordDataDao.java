package app.dao.dao_interfaces;

import app.model.Category;
import app.model.PasswordData;
import javafx.collections.ObservableList;

/**
 * Created by Antic on 15.08.2016.
 */
public interface IPasswordDataDao
{
  public ObservableList<PasswordData> getAllPasswordDataByCategory(Category category);

  public void addNewPasswordData(PasswordData passwordData) throws Exception;

  public void updatePasswordData(PasswordData passwordData) throws Exception;

  public void deletePasswordData(PasswordData passwordData) throws Exception;

  public void deleteAllPasswordDatas();
}
