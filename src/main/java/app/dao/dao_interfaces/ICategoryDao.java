package app.dao.dao_interfaces;

import app.model.Archive;
import app.model.Category;
import javafx.collections.ObservableList;

/**
 * Created by Antic on 15.08.2016.
 */
public interface ICategoryDao
{
  public ObservableList<Category> getCategoriesByArchive(Archive archive);

  public Category getCategoryByCategoryId(int categoryId);

  public void addCategory(Category category) throws Exception;

  public void updateCategory(Category category) throws Exception;

  public void deleteCategory(Category category) throws Exception;
}
