package app.dao.dao_interfaces;

import app.model.Category;
import javafx.collections.ObservableList;

import java.util.Iterator;

/**
 * Created by Antic on 27.07.2016.
 */
public interface InterPersonDao
{
  public ObservableList<Category> getAllCategories();

  public void addCategory(Category category) throws Exception;

  public void updateCategory(Category category) throws Exception;

  public void deleteCategory(Category category) throws Exception;

  public void deleteAllCategories();

  public void cleanAndClose();

  public boolean checkLoginData(String username, String password);
}