package app.dao.mysql;

import app.dao.dao_interfaces.IArchiveDao;
import app.dao.util.AesUtil;
import app.model.Archive;
import com.google.common.hash.Hashing;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javax.inject.Singleton;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.sql.*;
import java.util.Base64;

/**
 * Created by Antic on 15.08.2016.
 */
@Singleton
public class MySqlArchiveDao implements IArchiveDao
{
  private final String TABLE_NAME = "password_archive";
  private static Connection conn;
  private ObservableList<String> archiveTitles = FXCollections.observableArrayList();

  protected String cryptoPassword;
  protected Archive currentArchive;

  public MySqlArchiveDao() throws Exception {
    conn = MySqlConnector.getInstance().getConnection();
  }

  @Override
  public ObservableList<String> getAllArchiveTitles() {
    Statement stmt = null;
    ResultSet rs = null;
    String query = null;
    boolean error = false;

    try {
      stmt = conn.createStatement();
      query = "select title from " + TABLE_NAME;
      rs = stmt.executeQuery(query);
      archiveTitles.clear();

      while (rs.next()) {
        String title = rs.getString("title");
        archiveTitles.add(title);
      }

    } catch (SQLException e) {
      error = true;
      System.err.println(e.getMessage());
    } finally {
      try {
        rs.close();
        stmt.close();
      } catch (SQLException e) {
        error = true;
        System.err.println(e.getMessage());
      }

      if (error) {
        System.exit(-1);
      }

      return archiveTitles;
    }
  }

  @Override
  public Archive checkLoginData(String archiveName, String password) {
    PreparedStatement preparedStatement = null;
    ResultSet rs = null;
    Archive archive = null;
    String query = null;
    String title = null;
    boolean error = false;
    String hashedPassword = Hashing.sha512().hashString(password, StandardCharsets.UTF_8).toString();

    try {
      query = "SELECT * FROM " + TABLE_NAME + " WHERE title=? AND aes_crypting_password=?";
      preparedStatement = conn.prepareStatement(query);
      preparedStatement.setString(1, archiveName);
      preparedStatement.setString(2, hashedPassword);

      rs = preparedStatement.executeQuery();

      if (rs.next()) {
        int id = rs.getInt("id");
        title = rs.getString("title");
        archive = new Archive(id, title);
      }

      cryptoPassword = password;
    } catch (SQLException e) {
      error = true;
      System.err.println(e.getMessage());
    } finally {
      try {
        preparedStatement.close();
        rs.close();
      } catch (SQLException e) {
        error = true;
        System.err.println(e.getMessage());
      }

      if (error) {
        System.exit(-1);
      }

      currentArchive = archive;
      return archive;
    }
  }

  @Override
  public Archive getArchiveByArchiveId(int archiveId) {
    PreparedStatement preparedStatement = null;
    ResultSet rs = null;
    Archive archive = null;
    String query = null;
    String title = null;
    boolean error = false;

    try {
      query = "SELECT * FROM " + TABLE_NAME + " WHERE id=?";
      preparedStatement = conn.prepareStatement(query);
      preparedStatement.setInt(1, archiveId);

      rs = preparedStatement.executeQuery();

      if (rs.next()) {
        int id = rs.getInt("id");
        title = rs.getString("title");
        archive = new Archive(id, title);
      }
    } catch (SQLException e) {
      error = true;
      System.err.println(e.getMessage());
    } finally {
      try {
        preparedStatement.close();
        rs.close();
      } catch (SQLException e) {
        error = true;
        System.err.println(e.getMessage());
      }

      if (error) {
        System.exit(-1);
      }

      return archive;
    }
  }
}
