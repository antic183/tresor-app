package app.dao.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Antic on 15.08.2016.
 */
public class MySqlConnector
{
  private final String URL;
  private final String USER_NAME;
  private final String PASSWORD;
  private Connection conn;
  private static MySqlConnector instance;

  protected MySqlConnector() {
    this.URL = "jdbc:mysql://localhost/password_tresor";  //this.URL = "jdbc:mysql://localhost/tresor_app";
    this.USER_NAME = "tresor_app_user";
    this.PASSWORD = "1234";
  }

  private void connect(){
    try {
      conn = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
    } catch (SQLException e) {
      System.err.println(e.getMessage());
      System.exit(-1);
    }
  }

  public void closeConnection() throws SQLException {
    if (conn != null) {
      conn.close();
    }
  }

  public static MySqlConnector getInstance() throws Exception {
    if (instance == null) {
      instance = new MySqlConnector();
    }

    return instance;
  }

  protected final Connection getConnection() throws SQLException {
    if (conn == null) {
      connect();
    }

    return conn;
  }
}
