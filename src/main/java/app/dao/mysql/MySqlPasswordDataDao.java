package app.dao.mysql;

import app.Util.TreeHelper;
import app.dao.dao_interfaces.IPasswordDataDao;
import app.dao.util.AesUtil;
import app.model.Archive;
import app.model.app_Settings.AppSettings;
import app.model.Category;
import app.model.PasswordData;
import com.google.inject.Inject;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javax.inject.Singleton;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antic on 18.08.2016.
 */
@Singleton
public class MySqlPasswordDataDao implements IPasswordDataDao
{
  @Inject
  MySqlCategoryDao categoryDb;
  @Inject
  AppSettings appSettings;

  private String cryptoPassword;
  private final String TABLE_NAME = "password_data";
  private static Connection conn;
  private ObservableList<PasswordData> passwordDatas = FXCollections.observableArrayList();
  private Archive currentArchive;

  public MySqlPasswordDataDao() throws Exception {
    conn = MySqlConnector.getInstance().getConnection();
  }

  // TODO: move the in the class Util.TreeHelper
  // Todo: getAllIdsFromTreeHierarchy must be static in Util.TreeHelper


  @Override
  public ObservableList<PasswordData> getAllPasswordDataByCategory(Category category) {
    PreparedStatement preparedStatement = null;
    ResultSet rs = null;
    String query = null;
    String questionMark = "";

    List<Category> categoryList = categoryDb.getCategoriesByArchive(currentArchive);
    List<Integer> idList = new ArrayList<>();
    idList.add(category.getId());
    idList = appSettings.isShowPasswordDataFromSubcategories() ? TreeHelper.getAllIdsFromTreeHierarchy(categoryList, idList) : idList;

    for (int id : idList) {
      questionMark += idList.indexOf(id) < (idList.size() - 1) ? "?," : "?";
    }

    try {
      query = "SELECT * FROM " + TABLE_NAME + " WHERE category_id IN (" + questionMark + ")";
      preparedStatement = conn.prepareStatement(query);

      for (int id : idList) {
        int index = idList.indexOf(id);
        preparedStatement.setObject(index + 1, idList.get(index));
      }

      rs = preparedStatement.executeQuery();
      passwordDatas.clear();
      while (rs.next()) {
        int id = rs.getInt("id");
        String title = rs.getString("title");
        String username = AesUtil.decryptText(cryptoPassword, rs.getString("username"));
        String cryptedPassword = AesUtil.decryptText(cryptoPassword, rs.getString("aes_crypted_password"));
        int categoryId = rs.getInt("category_id");

        passwordDatas.add(new PasswordData(id, title, username, cryptedPassword, categoryDb.getCategoryByCategoryId(categoryId)));
      }
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      System.exit(-1);
    } finally {
      try {
        preparedStatement.close();
        rs.close();
      } catch (SQLException e) {
        System.out.println(e.getMessage());
        System.exit(-1);
      }

      return passwordDatas;
    }
  }

  @Override
  public void addNewPasswordData(PasswordData passwordData) throws Exception {
    PreparedStatement preparedStatement = null;
    ResultSet rs = null;
    String query = null;

    String title = passwordData.getTitle();
    String username = AesUtil.encryptText(cryptoPassword, passwordData.getUserName());
    String password = AesUtil.encryptText(cryptoPassword, passwordData.getPassword());
    int categoryId = passwordData.getCategory().getId();

    try {
      query = "INSERT INTO " + TABLE_NAME + "(title, username, aes_crypted_password, category_id) "
          + "values(?, ?, ?, ?)";

      preparedStatement = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
      preparedStatement.setString(1, title);
      preparedStatement.setString(2, username);
      preparedStatement.setString(3, password);
      preparedStatement.setInt(4, categoryId);
      preparedStatement.execute();

      rs = preparedStatement.getGeneratedKeys();
      if (rs.next()) {
        int last_inserted_id = rs.getInt(1);
        // set the PasswordData property id
        passwordData.setId(last_inserted_id);
      } else {
        throw new Exception("SQL Error: Insert into password data fail!");
      }
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      System.exit(-1);
    } finally {
      try {
        preparedStatement.close();
        rs.close();
      } catch (SQLException e) {
        System.out.println(e.getMessage());
        System.exit(-1);
      }
    }
  }

  @Override
  public void updatePasswordData(PasswordData passwordData) throws Exception {
    PreparedStatement preparedStatement = null;
    String query = null;

    String title = passwordData.getTitle();
    String username = AesUtil.encryptText(cryptoPassword, passwordData.getUserName());
    String password = AesUtil.encryptText(cryptoPassword, passwordData.getPassword());
    int id = passwordData.getId();
    int categoryId = passwordData.getCategory().getId();

    try {
      query = "UPDATE " + TABLE_NAME + " SET title=?, username=?, aes_crypted_password=?, category_id=?"
          + " WHERE id=? LIMIT 1";

      preparedStatement = conn.prepareStatement(query);
      preparedStatement.setString(1, title);
      preparedStatement.setString(2, username);
      preparedStatement.setString(3, password);
      preparedStatement.setInt(4, categoryId);
      preparedStatement.setInt(5, id);

      if (preparedStatement.executeUpdate() == 0) {
        throw new Exception("sql error. update query failed!");
      }
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      System.exit(-1);
    } finally {
      try {
        preparedStatement.close();
      } catch (SQLException e) {
        System.out.println(e.getMessage());
        System.exit(-1);
      }
    }
  }

  @Override
  public void deletePasswordData(PasswordData passwordData) throws Exception {
    PreparedStatement preparedStatement = null;
    String query = null;

    int id = passwordData.getId();

    try {
      query = "DELETE FROM " + TABLE_NAME + " WHERE id=? LIMIT 1";
      preparedStatement = conn.prepareStatement(query);
      preparedStatement.setInt(1, id);
      preparedStatement.execute();
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      System.exit(-1);
    } finally {
      try {
        preparedStatement.close();
      } catch (SQLException e) {
        System.out.println(e.getMessage());
        System.exit(-1);
      }
    }
  }

  @Override
  public void deleteAllPasswordDatas() {

  }

  @Inject
  private void addMySqlArchiveDao(MySqlArchiveDao archiveDao) {
    currentArchive = archiveDao.currentArchive;
    cryptoPassword = archiveDao.cryptoPassword;
  }

}
