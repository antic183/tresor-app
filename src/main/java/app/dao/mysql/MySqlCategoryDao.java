package app.dao.mysql;

import app.dao.dao_interfaces.ICategoryDao;
import app.model.Archive;
import app.model.Category;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antic on 15.08.2016.
 */
@Singleton
public class MySqlCategoryDao implements ICategoryDao
{
  private final String TABLE_NAME = "category";
  private static Connection conn;
  protected ObservableList<Category> categiries = FXCollections.observableArrayList();
  @Inject
  MySqlArchiveDao archiveDb;

  public MySqlCategoryDao() throws Exception {
    conn = MySqlConnector.getInstance().getConnection();
  }

//  public List<Category> getChildrenFromCategory(Category category) {
//    PreparedStatement preparedStatement = null;
//    ResultSet rs = null;
//    String query = null;
//    List<Category> childCategories = new ArrayList<>();
//
//    try {
//      query = "select @pv:=id as id, title, parent_id from category\n"
//          + "join\n"
//          + "(select @pv:=(?))tmp\n"
//          + "where parent_id=@pv";
//
//
//      query = "SELECT GROUP_CONCAT(c SEPARATOR ',') FROM (\n"
//          + "SELECT @pv:=(SELECT GROUP_CONCAT(id SEPARATOR ',') FROM category WHERE parent_id IN (@pv)) AS c FROM category\n"
//          + "JOIN\n"
//          + "(SELECT @pv:=1)tmp\n"
//          + "WHERE parent_id IN (@pv)) a";
//
//      preparedStatement = conn.prepareStatement(query);
//      preparedStatement.setInt(1, category.getId());
//      rs = preparedStatement.executeQuery();
//
//      while (rs.next()) {
//        int id = rs.getInt("id");
//        String title = rs.getString("title");
//        int parentId = rs.getInt("parent_id");
//        childCategories.add(new Category(id, title, parentId, category.getArchive()));
//      }
//    } catch (SQLException e) {
//      System.out.println(e.getMessage());
//      System.exit(-1);
//    } finally {
//      try {
//        preparedStatement.close();
//        rs.close();
//      } catch (SQLException e) {
//        e.printStackTrace();
//      }
//
//      return childCategories;
//    }
//  }

  @Override
  public Category getCategoryByCategoryId(int categoryId) {
    PreparedStatement preparedStatement = null;
    ResultSet rs = null;
    String query = null;
    Category category = null;

    try {
      query = "SELECT * FROM " + TABLE_NAME + " WHERE id=?";

      preparedStatement = conn.prepareStatement(query);
      preparedStatement.setInt(1, categoryId);

      rs = preparedStatement.executeQuery();
      if (rs.next()) {
        int id = rs.getInt("id");
        String title = rs.getString("title");
        int parentId = rs.getInt("parent_id");
        int archiveId = rs.getInt("archive_id");
        category = new Category(id, title, parentId, archiveDb.getArchiveByArchiveId(archiveId));
      }
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      System.exit(-1);
    } finally {
      try {
        preparedStatement.close();
        rs.close();
      } catch (SQLException e) {
        System.out.println(e.getMessage());
        System.exit(-1);
      }

      return category;
    }
  }

  @Override
  public ObservableList<Category> getCategoriesByArchive(Archive archive) {
    PreparedStatement preparedStatement = null;
    ResultSet rs = null;
    String query = null;

    try {
      query = "SELECT * FROM " + TABLE_NAME + " WHERE archive_id=?";
      preparedStatement = conn.prepareStatement(query);
      preparedStatement.setInt(1, archive.getId());
      rs = preparedStatement.executeQuery();
      categiries.clear();

      while (rs.next()) {
        int id = rs.getInt("id");
        String title = rs.getString("title");
        int parentId = rs.getInt("parent_id");
        categiries.add(new Category(id, title, parentId, archive));
      }
    } catch (SQLException e) {
      categiries = null;
      System.out.println(e.getMessage());
      System.exit(-1);
    } finally {
      try {
        preparedStatement.close();
        rs.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }

      return categiries;
    }
  }

  @Override
  public void addCategory(Category category) throws Exception {
    PreparedStatement preparedStatement = null;
    ResultSet rs = null;
    String query = null;

    try {
      query = "INSERT INTO " + TABLE_NAME + "(title, parent_id, archive_id) VALUES(?,?,?)";
      preparedStatement = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

      String title = category.getTitle();
      int parentId = category.getParentId();
      int archiveId = category.getArchive().getId();
      preparedStatement.setString(1, title);
      if (parentId != 0) {
        preparedStatement.setInt(2, parentId);
      } else {
        preparedStatement.setObject(2, null); // write null, when category is root
      }
      preparedStatement.setInt(3, archiveId);

      preparedStatement.execute();

      rs = preparedStatement.getGeneratedKeys();
      if (rs.next()) {
        int last_inserted_id = rs.getInt(1);
        // set the PasswordData property id
        category.setId(last_inserted_id);
      } else {
        throw new Exception("SQL Error: Insert into category fail!");
      }
    } catch (SQLException e) {
      categiries = null;
      System.out.println("Error:" + e.getMessage());
      System.exit(-1);
    } finally {
      try {
        preparedStatement.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public void updateCategory(Category category) throws Exception {

  }

  @Override
  public void deleteCategory(Category category) throws Exception {

  }

}
