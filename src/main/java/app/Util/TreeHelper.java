package app.Util;

import app.model.Category;
import javafx.scene.control.TreeItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antic on 31.08.2016.
 */
public class TreeHelper
{
  public static final List<Integer> getAllIdsFromTreeHierarchy(List<Category> categoryCollection, List<Integer> children) {
    List<Category> categoriesWhichHaveParents = new ArrayList<>();

    for (Category o : categoryCollection) {
      if (children.contains(o.getParentId())) {
        children.add(o.getId());
        categoriesWhichHaveParents.add(o);
      }
    }

    // update category collection
    categoryCollection.removeAll(categoriesWhichHaveParents);

    if (categoriesWhichHaveParents.size() > 0) {
      return getAllIdsFromTreeHierarchy(categoryCollection, children);
    }

    return children;
  }

  public static final void createChildTreeItems(List<Category> categoryList, List<TreeItem> rootItems) {
    List<Category> addedChildren = new ArrayList<>();
    List<TreeItem> nextRootItems = new ArrayList<>();

    for (TreeItem<Category> currentItem : rootItems) {
      for (Category child : categoryList) {
        if (currentItem.getValue().getId() == child.getParentId()) {
          TreeItem<Category> next = new TreeItem<>(child);
          currentItem.getChildren().add(next);
          nextRootItems.add(next);
          addedChildren.add(child);
        }
      }
    }

    // update category collection
    categoryList.removeAll(addedChildren);

    int categoryListSize = categoryList.size();
    if (categoryListSize > 0) {
      createChildTreeItems(categoryList, nextRootItems);
    }
  }
}
