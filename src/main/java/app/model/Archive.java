package app.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Antic on 15.08.2016.
 */
public class Archive
{
  private final int id;
  private StringProperty title;

  public Archive(int id, String title) {
    this.id = id;
    this.title = new SimpleStringProperty(title);
  }

  @Override
  public String toString() {
    return title.get();
  }

  public int getId() {
    return id;
  }

  public String getTitle() {
    return title.get();
  }

  public StringProperty titleProperty() {
    return title;
  }

  public void setTitle(String title) {
    this.title.set(title);
  }
}
