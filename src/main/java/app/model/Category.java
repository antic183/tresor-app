package app.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.List;

/**
 * Created by Antic on 26.07.2016.
 */
public class Category
{
  private int id;
  private StringProperty title;
  private int parentId;
  private Archive archive;

  public Category(int id, String title, int parentId, Archive archive) {
    this.id = id;
    this.title = new SimpleStringProperty(title);
    this.parentId = parentId;
    this.archive = archive;
  }

  public Category(String title, int parentId, Archive archive) {
    this.title = new SimpleStringProperty(title);
    this.parentId = parentId;
    this.archive = archive;
  }

  @Override
  public String toString() {
    return title.getValue();
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setTitle(String title) {
    this.title.set(title);
  }

  public String getTitle() {
    return title.get();
  }

  public StringProperty titleProperty() {
    return title;
  }

  public int getParentId() {
    return parentId;
  }

  public void setParentId(int parentId) {
    this.parentId = parentId;
  }

  public Archive getArchive() {
    return archive;
  }

  public void setArchive(Archive archive) {
    this.archive = archive;
  }
}
