package app.model.app_Settings;

/**
 * Created by Antic on 27.08.2016.
 */
public class AppSettings
{
  private boolean showPasswordDataFromSubcategories = false;

  public boolean isShowPasswordDataFromSubcategories() {
    return showPasswordDataFromSubcategories;
  }

  public void setShowPasswordDataFromSubcategories(boolean showPasswordDataFromSubcategories) {
    this.showPasswordDataFromSubcategories = showPasswordDataFromSubcategories;
  }

}
