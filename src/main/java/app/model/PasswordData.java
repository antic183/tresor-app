package app.model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Antic on 06.08.2016.
 */
public class PasswordData
{
  private int id;
  private StringProperty title;
  private StringProperty userName;
  private StringProperty password;

  private ObjectProperty<Category> category;

  public PasswordData(int id, String title, String userName, String password, Category category) {
    this.id = id;
    this.title = new SimpleStringProperty(title);
    this.userName = new SimpleStringProperty(userName);
    this.password = new SimpleStringProperty(password);
    this.category = new SimpleObjectProperty<>(category);
  }

  public PasswordData(String title, String userName, String password, Category category) {
    this.title = new SimpleStringProperty(title);
    this.userName = new SimpleStringProperty(userName);
    this.password = new SimpleStringProperty(password);
    this.category = new SimpleObjectProperty<>(category);
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTitle() {
    return title.get();
  }

  public StringProperty titleProperty() {
    return title;
  }

  public void setTitle(String title) {
    this.title.set(title);
  }

  public String getUserName() {
    return userName.get();
  }

  public StringProperty userNameProperty() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName.set(userName);
  }

  public String getPassword() {
    return password.get();
  }

  public StringProperty passwordProperty() {
    return password;
  }

  public void setPassword(String password) {
    this.password.set(password);
  }

  public Category getCategory() {
    return category.get();
  }

  public ObjectProperty<Category> categoryProperty() {
    return category;
  }

  public StringProperty categoryTitlePropertyByDelegation() {
    return category.get().titleProperty();
  }

  public void setCategory(Category category) {
    this.category.set(category);
  }
}
