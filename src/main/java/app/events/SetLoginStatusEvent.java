package app.events;

import app.model.Archive;

/**
 * Created by Antic on 13.08.2016.
 */
public class SetLoginStatusEvent
{
  private boolean isLoggedIn;
  private Archive archive;

  public SetLoginStatusEvent(boolean isLoggedIn, Archive archive) {
    this.isLoggedIn = isLoggedIn;
    this.archive = archive;
  }

  public boolean isLoggedIn() {
    return isLoggedIn;
  }

  public Archive getArchive() {
    return archive;
  }
}
