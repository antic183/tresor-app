package app.events.app_content;

/**
 * Created by Antic on 27.08.2016.
 */
public class SetAppSettingsChangedEvent
{
  private boolean refreshContent;

  public SetAppSettingsChangedEvent(boolean refreshContent) {
    this.refreshContent = refreshContent;
  }

  public boolean isContentToRefresh() {
    return refreshContent;
  }
}
