package app.events.app_content;

import app.model.PasswordData;

/**
 * Created by Antic on 21.08.2016.
 */
public class SetPasswordDataEdittedEvent
{
  private PasswordData passwordData;
  private boolean newEntry;

  public SetPasswordDataEdittedEvent(PasswordData passwordData, boolean newEntry) {
    this.passwordData = passwordData;
    this.newEntry = newEntry; // new entry or eddited existing passwordData ??
  }

  public boolean isNewEntry() {
    return newEntry;
  }

  public PasswordData getPasswordData() {
    return passwordData;
  }
}
