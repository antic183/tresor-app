package app.events.app_content;

import app.model.PasswordData;

/**
 * Created by Antic on 08.08.2016.
 */
public class SetPasswordDataEditEvent
{
  private PasswordData passwordData;
  private boolean newEntry;

  public SetPasswordDataEditEvent(PasswordData passwordData, boolean newEntry) {
    this.passwordData = passwordData;
    this.newEntry = newEntry;
  }

  public SetPasswordDataEditEvent(boolean newEntry) {
    this.newEntry = newEntry;
  }

  public PasswordData getPasswordData() {
    return passwordData;
  }

  public boolean isNewEntry() {
    return newEntry;
  }
}
