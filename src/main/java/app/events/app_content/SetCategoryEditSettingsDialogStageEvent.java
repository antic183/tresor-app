package app.events.app_content;

import javafx.stage.Stage;

/**
 * Created by Antic on 08.08.2016.
 */
public class SetCategoryEditSettingsDialogStageEvent
{
  private Stage dialogStage;

  public SetCategoryEditSettingsDialogStageEvent(Stage stage) {
    dialogStage = stage;
  }

  public Stage getDialogStage() {
    return dialogStage;
  }
}
