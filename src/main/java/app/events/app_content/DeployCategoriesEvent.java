package app.events.app_content;

import app.model.Archive;
import app.model.Category;

import java.util.List;

/**
 * Created by Antic on 31.08.2016.
 */
public class DeployCategoriesEvent
{
  private Archive archive;
  private List<Category> categoryList;

  public DeployCategoriesEvent(Archive archive, List<Category> categoryList) {
    this.archive = archive;
    this.categoryList = categoryList;
  }

  public Archive getArchive() {
    return archive;
  }

  public List<Category> getCategoryList() {
    return categoryList;
  }
}
