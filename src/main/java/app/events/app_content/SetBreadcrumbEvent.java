package app.events.app_content;

/**
 * Created by Antic on 07.08.2016.
 */
public class SetBreadcrumbEvent
{
  private String breadcrump = "";

  public SetBreadcrumbEvent(String breadcrump) {
    this.breadcrump = breadcrump;
  }

  public String getBreadcrump() {
    return breadcrump;
  }
}
