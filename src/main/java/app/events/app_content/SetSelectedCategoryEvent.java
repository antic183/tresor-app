package app.events.app_content;

import app.model.Category;

import java.util.List;

/**
 * Created by Antic on 07.08.2016.
 */
public class SetSelectedCategoryEvent
{
  private Category currentCategory;
  private List<Category> categoryList;

  public SetSelectedCategoryEvent(Category category, List<Category> categoryList) {
    currentCategory = category;
    this.categoryList = categoryList;
  }

  public Category getSelectedCategory() {
    return currentCategory;
  }

  public List<Category> getCategoryList() {
    return categoryList;
  }
}
