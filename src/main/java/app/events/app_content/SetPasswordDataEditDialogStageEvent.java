package app.events.app_content;

import javafx.stage.Stage;

/**
 * Created by Antic on 08.08.2016.
 */
public class SetPasswordDataEditDialogStageEvent
{
  private Stage dialogStage;

  public SetPasswordDataEditDialogStageEvent(Stage stage) {
    dialogStage = stage;
  }

  public Stage getDialogStage() {
    return dialogStage;
  }
}
