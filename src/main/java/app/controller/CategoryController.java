package app.controller;

import app.Util.TreeHelper;
import app.dao.dao_interfaces.ICategoryDao;
import app.events.app_content.DeployCategoriesEvent;
import app.events.app_content.SetBreadcrumbEvent;
import app.events.SetLoginStatusEvent;
import app.events.SetPrimaryStageEvent;
import app.events.app_content.SetSelectedCategoryEvent;
import app.model.Archive;
import app.model.Category;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.*;

/**
 * Created by Antic on 26.07.2016.
 */
public class CategoryController
{
  @FXML
  private TreeView<Category> treeView;
  @FXML
  private Label title;

  @Inject
  private EventBus eventBus;
  @Inject
  private Stage rootStage;
  @Inject
  private ICategoryDao categoryDb;

  private Archive currentArchive;
  private TreeItem<Category> hiddenFirstItem;
  private List<Category> categoryList;
  private TreeItem<Category> selectedTreeItem;

  @FXML
  public void initialize() {
    System.out.println("initialize " + this.getClass().getSimpleName());
  }

  @FXML
  private void btnNewCategory() {
    TextInputDialog dialog = new TextInputDialog();
    dialog.setTitle("New Category");
    dialog.setHeaderText("New Category");
    dialog.setContentText("Please enter the new Catagery name:");

    Optional<String> result = dialog.showAndWait();
    if (result.isPresent()) {
      String categoryName = result.get().trim();
      Category selectedCategory = selectedTreeItem != null ? selectedTreeItem.getValue() : null;
      int parentId = selectedCategory != null ? selectedCategory.getId() : 0;
      Category newCategory = new Category(categoryName, parentId, currentArchive);
      try {
        categoryDb.addCategory(newCategory);
        categoryList.add(newCategory);
        // selectedTreeItem is null when the category is a root element
        if (selectedTreeItem != null) {
          selectedTreeItem.getChildren().add(new TreeItem<Category>(newCategory));
        } else {
          hiddenFirstItem.getChildren().add(new TreeItem<Category>(newCategory));
        }
        eventBus.post(new SetSelectedCategoryEvent(selectedCategory, categoryList));
      } catch (Exception e) {
        System.err.println(e.getMessage());
      }
    }
  }

  @Subscribe
  private void addStage(SetPrimaryStageEvent eventObject) throws IOException {
    rootStage = eventObject.getStage();
  }

  @Subscribe
  private void userIsLoggedIn(SetLoginStatusEvent eventObject) {
    if (eventObject.isLoggedIn()) {
      currentArchive = eventObject.getArchive();
      initTreeView();
      initSelectionListener();
    }
  }

  private void initTreeView() {
    // the hidden first item do not get an id (default = 0)!
    hiddenFirstItem = new TreeItem<>(new Category("not visible root Item", 0, currentArchive));
    treeView.setShowRoot(false); // hide hiddenFirstItem

    categoryList = new ArrayList<>();
    categoryList.addAll(categoryDb.getCategoriesByArchive(currentArchive));
    List<TreeItem> rootItems = new ArrayList<>();
    rootItems.add(hiddenFirstItem);

    // categoryList must be a copy!
    TreeHelper.createChildTreeItems(new ArrayList<>(categoryList), rootItems);
    treeView.setRoot(hiddenFirstItem);

    // categoryList must be a copy!
    eventBus.post(new DeployCategoriesEvent(currentArchive, new ArrayList<>(categoryList)));
  }

  private void initSelectionListener() {
    treeView.getSelectionModel().selectedItemProperty().addListener(
        (observable, oldValue, newValue) ->
        {
          String breadcrumb = createSelectedItemBreadcrumb(newValue, "");
          eventBus.post(new SetBreadcrumbEvent(breadcrumb));
          selectedTreeItem = newValue;
          eventBus.post(new SetSelectedCategoryEvent(newValue.getValue(), categoryList));
        }
    );
  }

  private String createSelectedItemBreadcrumb(TreeItem<Category> item, String headNode) {
    String itemValue = "";
    boolean itemHasParent = item.getParent() != null ? true : false;
    // don't show hiddenFirstItem in breadcrump
    if (!item.equals(hiddenFirstItem)) {
      itemValue = item.getValue() + "/" + headNode;
    } else {
      itemValue = headNode;
    }

    if (itemHasParent) {
      return createSelectedItemBreadcrumb(item.getParent(), itemValue);
    }

    return itemValue.replaceAll("/$", "");
  }

}
