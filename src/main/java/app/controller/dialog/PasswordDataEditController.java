package app.controller.dialog;

import app.Util.TreeHelper;
import app.dao.dao_interfaces.ICategoryDao;
import app.dao.dao_interfaces.IPasswordDataDao;
import app.events.app_content.*;
import app.model.Category;
import app.model.PasswordData;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import com.sun.javafx.scene.control.skin.TextFieldSkin;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antic on 08.08.2016.
 */
public class PasswordDataEditController
{
  @FXML
  private TextField txtFieldTitle;
  @FXML
  private TextField txtFieldUsername;
  @FXML
  private PasswordField passwordFieldPassword;
  @FXML
  private CheckBox checkBoxShowPassword;
  @FXML
  private AnchorPane anchorPane;
  @FXML
  private ComboBox<Category> comboCategory;
  @FXML
  private TreeView<Category> treeView;

  @Inject
  private EventBus eventBus;
  @Inject
  private IPasswordDataDao passwordDataDb;
  @Inject
  private ICategoryDao categoryDb;

  private Stage dialogStage;
  private boolean newEntry;
  private PasswordData selectedPasswordData;
  private Category defaultCategory;

  private class PasswordSkin extends TextFieldSkin
  {
    public PasswordSkin(TextField textField) {
      super(textField);
    }

    @Override
    protected String maskText(String txt) {
      if (checkBoxShowPassword.isSelected()) {
        return txt;
      } else {
        return super.maskText(txt);
      }
    }

    protected void update() {
      super.updateCaretOff();
    }
  }

  @FXML
  public void initialize() {
    PasswordSkin passwordSkin = new PasswordSkin(passwordFieldPassword);
    passwordFieldPassword.setSkin(passwordSkin);

    checkBoxShowPassword.selectedProperty().addListener(
        (observable, oldValue, newValue) -> {
          // update
          passwordFieldPassword.setText(passwordFieldPassword.getText());
        }
    );

    treeView.setShowRoot(false); // hide hiddenFirstItem
    System.out.println("initialize " + this.getClass().getSimpleName());
  }

  @FXML
  private void btnSaveData() {
    String title = txtFieldTitle.getText();
    String username = txtFieldUsername.getText();
    String password = passwordFieldPassword.getText();
    Category category = comboCategory.getValue();

    if (newEntry) {
      PasswordData newPasswordData = new PasswordData(title, username, password, category);
      try {
        passwordDataDb.addNewPasswordData(newPasswordData);
        eventBus.post(new SetPasswordDataEdittedEvent(newPasswordData, true));
        closeDialog();
      } catch (Exception e) {
        System.out.println("FEHLERMEDLUNGSDIALOG:" + e.getMessage());
      }
    } else {
      selectedPasswordData.setTitle(title);
      selectedPasswordData.setUserName(username);
      selectedPasswordData.setPassword(password);
      selectedPasswordData.setCategory(category);
      try {
        passwordDataDb.updatePasswordData(selectedPasswordData);
        eventBus.post(new SetPasswordDataEdittedEvent(selectedPasswordData, false));
        closeDialog();
      } catch (Exception e) {
        System.out.println("FEHLERMEDLUNGSDIALOG:" + e.getMessage());
      }
    }
  }

  private void closeDialog() {
    dialogStage.close();
  }

  @FXML
  private void btnCancel() {
    closeDialog();
  }

  // posted in PasswordDataController
  @Subscribe
  private void addSelectedPasswordData(SetPasswordDataEditEvent eventObject) {
    newEntry = eventObject.isNewEntry();
    checkBoxShowPassword.setSelected(false);
    Platform.runLater(() -> anchorPane.requestFocus());
    if (!newEntry) {
      selectedPasswordData = eventObject.getPasswordData();

      // override category @setDefaultCategoryBySelection()
      Category categoryBySelectedPasswordData = selectedPasswordData.getCategory();
      comboCategory.setValue(categoryBySelectedPasswordData);

      txtFieldTitle.setText(selectedPasswordData.getTitle());
      txtFieldUsername.setText(selectedPasswordData.getUserName());
      passwordFieldPassword.setText(selectedPasswordData.getPassword());
    } else {
      selectedPasswordData = null;
      txtFieldTitle.clear();
      txtFieldUsername.clear();
      passwordFieldPassword.clear();
      comboCategory.setValue(defaultCategory);
    }
  }

  @Subscribe
  private void addDialogStage(SetPasswordDataEditDialogStageEvent eventObject) {
    dialogStage = eventObject.getDialogStage();
  }

  @Subscribe
  private void setDefaultCategoryByCategorySelection(SetSelectedCategoryEvent eventObject) {
    comboCategory.getItems().clear();
    comboCategory.getItems().addAll(eventObject.getCategoryList());

    defaultCategory = eventObject.getSelectedCategory();
    comboCategory.setValue(defaultCategory);
  }

  @Subscribe
  private void setRootTreeItem(DeployCategoriesEvent eventObject) {
    System.out.println("size = " + eventObject.getCategoryList().size());

    TreeItem<Category> hiddenRootItem = new TreeItem<>(new Category("hidden", 0, eventObject.getArchive()));
    List<TreeItem> rootItems = new ArrayList<>();
    rootItems.add(hiddenRootItem);
    TreeHelper.createChildTreeItems(eventObject.getCategoryList(), rootItems);
    treeView.setRoot(hiddenRootItem);
  }


}
