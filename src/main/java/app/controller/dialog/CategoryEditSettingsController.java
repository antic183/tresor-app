package app.controller.dialog;

import app.events.app_content.SetCategoryEditSettingsDialogStageEvent;

import app.events.app_content.SetAppSettingsChangedEvent;
import app.model.app_Settings.AppSettings;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.stage.Stage;

/**
 * Created by Antic on 27.08.2016.
 */
public class CategoryEditSettingsController
{
  @Inject
  private AppSettings appSettings;
  @Inject
  private EventBus eventBus;

  @FXML
  CheckBox checkBoxShowingPasswordDataFromSubcategories;

  private Stage dialogStage;

  @FXML
  public void initialize() {
    System.out.println("init: " + getClass().getSimpleName());
  }

  @FXML
  private void btnClose() {
    closeDialog();
  }

  private void closeDialog() {
    dialogStage.close();
  }

  @FXML
  private void changeShowingPasswordDataFromSubcategories() {
    boolean enabled = appSettings.isShowPasswordDataFromSubcategories();
    if (enabled) {
      appSettings.setShowPasswordDataFromSubcategories(false);
    } else {
      appSettings.setShowPasswordDataFromSubcategories(true);
    }

    eventBus.post(new SetAppSettingsChangedEvent(true));
  }

  // posted in RootController
  @Subscribe
  private void addDialogStage(SetCategoryEditSettingsDialogStageEvent eventObject) {
    dialogStage = eventObject.getDialogStage();
    initSettings();
  }

  private void initSettings() {
    boolean enabled = appSettings.isShowPasswordDataFromSubcategories();
    checkBoxShowingPasswordDataFromSubcategories.selectedProperty().setValue(enabled);
  }
}
