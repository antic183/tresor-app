package app.controller.cell_renderer;

import app.model.PasswordData;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.HBox;

/**
 * Created by Antic on 27.08.2016.
 */
public class PasswordCell extends TableCell<PasswordData, String>
{
  private String hiddenValue;
  private HBox hbox;
  private TextField field;
  private String txtShow;
  private String txtHide;
  private Button btn;

  public PasswordCell() {
    hiddenValue = "*******";
    hbox = new HBox(15);
    field = new TextField(hiddenValue);
    txtShow = "Show";
    txtHide = "Hide";
    btn = new Button(txtShow);
    field.setMinWidth(100.0);
    field.setDisable(true);
    field.setEditable(false);
    btn.setMinWidth(50.0);
    hbox.getChildren().addAll(field, btn);
  }

  @Override
  protected void updateItem(String item, boolean empty) {
    super.updateItem(item, empty);

    if (!empty) {
      setGraphic(hbox);
      field.setText(hiddenValue);
      field.setDisable(true);
      btn.setText(txtShow);

      btn.setOnMouseClicked((event) -> {
        if (event.getButton() == MouseButton.PRIMARY) {
          if (field.getText().equals(hiddenValue)) {
            field.setText(item.toString());
            btn.setText(txtHide);
            field.setDisable(false);
          } else {
            field.setText(hiddenValue);
            btn.setText(txtShow);
            field.setDisable(true);
          }
        }
      });
    } else {
      setText(null);
      setGraphic(null);
    }
  }
};