package app.controller;

import app.events.SetPrimaryStageEvent;
import app.events.app_content.SetCategoryEditSettingsDialogStageEvent;
import app.injector.FXMLLoaderAndInjector;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by Antic on 27.08.2016.
 */
public class RootController
{
  @Inject
  private EventBus eventBus;

  private Stage rootStage;
  private Stage dialogStage;

  @Subscribe
  private void addStage(SetPrimaryStageEvent eventObject) throws IOException {
    rootStage = eventObject.getStage();
    initCategoryEditSettingsDialog();
  }

  private void initCategoryEditSettingsDialog() throws IOException {
    dialogStage = new Stage();
    dialogStage.initModality(Modality.WINDOW_MODAL);
    dialogStage.initOwner(rootStage);
    Scene scene = new Scene(FXMLLoaderAndInjector.loadNodeRegisterAndInjectDependencies(AnchorPane.class, "/view/dialogs/CategoryEditSettingsDialog.fxml"));
    dialogStage.setResizable(false);
    dialogStage.setScene(scene);
    dialogStage.setTitle("Edit category settings");

    eventBus.post(new SetCategoryEditSettingsDialogStageEvent(dialogStage));
  }

  @FXML
  private void showCategoryEditSettingsDialog() throws IOException {
    dialogStage.showAndWait();
  }

  @FXML
  public void initialize() {
    System.out.println("init: " + getClass().getSimpleName());
  }

}
