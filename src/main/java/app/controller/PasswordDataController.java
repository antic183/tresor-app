package app.controller;

import app.controller.cell_renderer.PasswordCell;
import app.dao.dao_interfaces.ICategoryDao;
import app.dao.dao_interfaces.IPasswordDataDao;
import app.events.*;
import app.events.app_content.*;
import app.injector.FXMLLoaderAndInjector;
import app.model.Category;
import app.model.PasswordData;
import app.model.app_Settings.AppSettings;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;

/**
 * Created by Antic on 26.07.2016.
 */
public class PasswordDataController
{
  @FXML
  private TableView<PasswordData> userDataTable;
  @FXML
  private TableColumn<PasswordData, String> titleColumn;
  @FXML
  private TableColumn<PasswordData, String> userNameColumn;
  @FXML
  private TableColumn<PasswordData, String> passwordColumn;
  @FXML
  private TableColumn<PasswordData, String> categoryColumn;
  @FXML
  private Label breadcrumb;
  @FXML
  private Button btnEdit;
  @FXML
  private Button btnDelete;
  @FXML
  private Button btnAdd;

  @Inject
  private EventBus eventBus;
  @Inject
  private IPasswordDataDao passwordDataDb;
  @Inject
  private ICategoryDao categoryDb;
  @Inject
  private AppSettings appSettings;

  private Stage rootStage;
  private Stage dialogStage;
  private PasswordData selectedPasswordData;
  private ObservableList<PasswordData> passwordDataList;

  private Category defaultCategory;

  @FXML
  public void initialize() {
    System.out.println("initialize " + this.getClass().getSimpleName());

    btnAdd.setDisable(true);
    disableEditButtons();
  }

  @FXML
  private void newUserData() throws IOException {
    eventBus.post(new SetPasswordDataEditEvent(true));
    showPasswordDataEditDialog("New Password data");
  }

  @FXML
  private void editUserData() throws IOException {
    if (selectedPasswordData != null) {
      eventBus.post(new SetPasswordDataEditEvent(selectedPasswordData, false));
      showPasswordDataEditDialog("Edit Password data");
    }
  }

  @FXML
  private void deleteUserData() throws Exception {
    passwordDataDb.deletePasswordData(selectedPasswordData);
    passwordDataList.remove(selectedPasswordData);
    if (passwordDataList.size() == 0) {
      disableEditButtons();
    }
  }

  @Subscribe
  private void addStage(SetPrimaryStageEvent eventObject) throws IOException {
    rootStage = eventObject.getStage();

    rootStage.setOnCloseRequest(new EventHandler<WindowEvent>()
    {
      public void handle(WindowEvent we) {
        System.out.println("Stage is closing -> IMPLEMENT IT!!!!!");
//        db.cleanAndClose();
      }
    });

    initPasswordDataEditDialog();
    initTable();
    addCellSelectionListener();
  }

  private void addCellSelectionListener() {
    userDataTable.getSelectionModel().selectedItemProperty().addListener(
        (observable, oldValue, newValue) -> {
          selectedPasswordData = newValue;
          enableEditButtons();
        }
    );
  }

  private void initPasswordDataEditDialog() throws IOException {
    dialogStage = new Stage();
    dialogStage.initModality(Modality.WINDOW_MODAL);
    dialogStage.initOwner(rootStage);
    Scene scene = new Scene(FXMLLoaderAndInjector.loadNodeRegisterAndInjectDependencies(AnchorPane.class, "/view/dialogs/PasswordDataEditDialog.fxml"));
    dialogStage.setResizable(false);
    dialogStage.setScene(scene);

    eventBus.post(new SetPasswordDataEditDialogStageEvent(dialogStage));
  }

  private void showPasswordDataEditDialog(String title) throws IOException {
    dialogStage.setTitle(title);
    dialogStage.showAndWait();
  }

  private void disableEditButtons() {
    btnEdit.setDisable(true);
    btnDelete.setDisable(true);
  }

  private void enableEditButtons() {
    btnEdit.setDisable(false);
    btnDelete.setDisable(false);
  }

  // posted in CategoryController
  @Subscribe
  private void setBreadcrumb(SetBreadcrumbEvent eventObject) {
    btnAdd.setDisable(false);
    breadcrumb.setText(eventObject.getBreadcrump());
  }

  // posted in CategoryController
  @Subscribe
  private void setPasswordListByCategory(SetSelectedCategoryEvent eventObject) {
    defaultCategory = eventObject.getSelectedCategory();
    passwordDataList = passwordDataDb.getAllPasswordDataByCategory(defaultCategory);
    userDataTable.setItems(passwordDataList);
    disableEditButtons();
    refreshPasswordDataList();
  }

  @Subscribe
  private void appSettingWasChanged(SetAppSettingsChangedEvent eventObject) {
    if (eventObject.isContentToRefresh()) {
      refreshPasswordDataList();
      manageOptionalColumns();
    }
  }

  // posted in PasswordDataEditController
  @Subscribe
  private void passwordDataEditted(SetPasswordDataEdittedEvent eventObject) {
    PasswordData tmpPasswordData = eventObject.getPasswordData();

    if (eventObject.isNewEntry()) {
      passwordDataList.add(tmpPasswordData);
    } else {
      int index;
      if ((index = passwordDataList.indexOf(tmpPasswordData)) != -1) {
        passwordDataList.set(index, tmpPasswordData);
      }
    }

    refreshPasswordDataList();
  }

  private void initTable() {
    titleColumn.setCellValueFactory(cellData -> cellData.getValue().titleProperty());
    userNameColumn.setCellValueFactory(cellData -> cellData.getValue().userNameProperty());
    passwordColumn.setCellValueFactory(cellData -> cellData.getValue().passwordProperty());
    passwordColumn.setCellFactory(column -> new PasswordCell());

    categoryColumn.setCellValueFactory(cellData -> cellData.getValue().categoryTitlePropertyByDelegation());

    manageOptionalColumns();
  }

  private void manageOptionalColumns() {
    if (appSettings.isShowPasswordDataFromSubcategories()) {
      categoryColumn.setVisible(true);
    } else {
      categoryColumn.setVisible(false);
    }
  }

  private void refreshPasswordDataList() {
    passwordDataList = passwordDataDb.getAllPasswordDataByCategory(defaultCategory);
    disableEditButtons();
  }

}
