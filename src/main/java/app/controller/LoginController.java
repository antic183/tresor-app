package app.controller;

import app.dao.dao_interfaces.IArchiveDao;
import app.events.SetLoginStatusEvent;
import app.events.SetPrimaryStageEvent;
import app.injector.FXMLLoaderAndInjector;
import app.model.Archive;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by Antic on 10.08.2016.
 */
public class LoginController
{

  @FXML
  private ComboBox<String> comboArchive;
  @FXML
  private PasswordField passwordFieldPassword;
  @FXML
  private AnchorPane anchorPane;

  @Inject
  private EventBus eventBus;
  @Inject
  private IArchiveDao archiveDb;

  private Stage rootStage;

  private BorderPane borderPaneMenu;      // root
  private SplitPane splitPane;            // split left and right
  private AnchorPane anchorPaneTreeView;  // left
  private AnchorPane anchorPaneOverview;  // right

  @FXML
  public void initialize() {
    System.out.println("init: " + getClass().getSimpleName());
    Platform.runLater(() -> anchorPane.requestFocus());
  }

  @FXML
  private void loginEvent() {
    String archiveTitle = comboArchive.getSelectionModel().getSelectedItem() != null ? comboArchive.getSelectionModel().getSelectedItem() : "";
    String password = passwordFieldPassword.getText() != null ? passwordFieldPassword.getText().trim() : "";

    if (!archiveTitle.equals("") && !password.equals("")) {
      Archive archive = archiveDb.checkLoginData(archiveTitle, password);
      if (archive != null) {
        initAppAfterSuccessfullLogin();
        eventBus.post(new SetLoginStatusEvent(true, archive));
      }
    }
  }

  @Subscribe
  private void addStage(SetPrimaryStageEvent eventObject) throws IOException {
    rootStage = eventObject.getStage();
    rootStage.setResizable(false);
    comboArchive.setItems(archiveDb.getAllArchiveTitles());
    eventBus.unregister(this);
  }

  private void initAppAfterSuccessfullLogin() {
    try {
      rootStage.hide();
      initAppMenu();
      initSplitPane();

      initTree();
      initOverview();
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
  }

  private void initAppMenu() throws IOException {
    borderPaneMenu = FXMLLoaderAndInjector.loadNodeRegisterAndInjectDependencies(BorderPane.class, "/view/app_content/RootLayout.fxml");
    Scene scene = new Scene(borderPaneMenu);
    rootStage.setScene(scene);
    rootStage.centerOnScreen();
    rootStage.setResizable(true);
    rootStage.show();
  }

  private void initSplitPane() {
    splitPane = new SplitPane();
    splitPane.setOrientation(Orientation.HORIZONTAL);
    splitPane.setDividerPositions(0.3f, 0.6f, 0.9f);

    borderPaneMenu.setCenter(splitPane);
  }

  private void initTree() throws IOException {
    anchorPaneTreeView = FXMLLoaderAndInjector.loadNodeRegisterAndInjectDependencies(AnchorPane.class, "/view/app_content/CategoryView.fxml");
    splitPane.getItems().add(anchorPaneTreeView);
  }

  private void initOverview() throws IOException {
    anchorPaneOverview = FXMLLoaderAndInjector.loadNodeRegisterAndInjectDependencies(AnchorPane.class, "/view/app_content/PasswordData.fxml");
    FXMLLoaderAndInjector.permittStageToReceivers(new SetPrimaryStageEvent(rootStage));
    splitPane.getItems().add(anchorPaneOverview);
  }
}
