package app;

import app.events.SetPrimaryStageEvent;
import app.injector.FXMLLoaderAndInjector;
import app.injector.InjectorConfiguration;
import com.google.inject.Guice;
import com.google.inject.Injector;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by Antic on 25.07.2016.
 */
public class Main extends Application
{
  private Stage stage;

  @Override
  public void start(Stage myStage) throws Exception {
    this.stage = myStage;

    initInjector();
    initApp();
  }

  private void initInjector() {
    Injector injector = Guice.createInjector(new InjectorConfiguration());
    injector.getInstance(FXMLLoaderAndInjector.class);
  }

  private void initApp() throws IOException {
    BorderPane borderPaneLogin = FXMLLoaderAndInjector.loadNodeRegisterAndInjectDependencies(BorderPane.class, "/view/login/Login.fxml");
    FXMLLoaderAndInjector.permittStageToReceivers(new SetPrimaryStageEvent(stage));

    Scene scene = new Scene(borderPaneLogin);
    stage.setScene(scene);
    stage.show();
  }

  public static void main(String[] args) {
    launch(args);
  }
}