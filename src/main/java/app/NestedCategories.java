package app;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antic on 30.08.2016.
 */
public class NestedCategories
{

  public static void main(String[] args) {
    new NestedCategories();
  }

  NestedCategories() {
    List<Category> fullCategoryList = new ArrayList<>();
    fullCategoryList.add(new Category(1, 0));
    fullCategoryList.add(new Category(2, 0));
    fullCategoryList.add(new Category(3, 2));
    fullCategoryList.add(new Category(4, 1));
    fullCategoryList.add(new Category(5, 1));
    fullCategoryList.add(new Category(6, 4));
    fullCategoryList.add(new Category(8, 3));
    fullCategoryList.add(new Category(9, 6));
    fullCategoryList.add(new Category(7, 2));
    fullCategoryList.add(new Category(11, 5));
    fullCategoryList.add(new Category(14, 5));
    fullCategoryList.add(new Category(15, 11));
    fullCategoryList.add(new Category(41, 4));
    fullCategoryList.add(new Category(98, 4));
    fullCategoryList.add(new Category(105, 4));
    fullCategoryList.add(new Category(107, 98));

    List<Integer> rootId = new ArrayList<>();
    rootId.add(1);
    List<Integer> result = getAllIdsFromTreeHierarchy(fullCategoryList, rootId);
    System.out.println(result);
  }

  public List<Integer> getAllIdsFromTreeHierarchy(List<Category> categoryCollection, List<Integer> parents) {
    List<Category> categoriesWhichHaveParents = new ArrayList<>();

    // fill next parents
    for (Category o : categoryCollection) {
      if (parents.contains(o.getParentId())) {
        parents.add(o.getId());
        categoriesWhichHaveParents.add(o);
      }
    }

    // update category collection (remove added ids from collection)
    categoryCollection.removeAll(categoriesWhichHaveParents);

    if (categoriesWhichHaveParents.size() > 0) {
      //recursive call, while a parent category exists in collection
      return getAllIdsFromTreeHierarchy(categoryCollection, parents);
    }

    return parents;
  }

  private class Category
  {
    int id;
    int parentId;

    public Category(int id, int parentId) {
      this.id = id;
      this.parentId = parentId;
    }

    public int getId() {
      return id;
    }

    public int getParentId() {
      return parentId;
    }
  }
}
