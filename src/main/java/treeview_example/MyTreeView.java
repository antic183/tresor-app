package treeview_example;

import java.util.Arrays;
import java.util.List;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Callback;

import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;

public class MyTreeView extends Application
{
  List<Employee> employees = Arrays.<Employee>asList(
      new Employee("Hans", "Meier"),
      new Employee("Kurt", "Weissenberger"));

  TreeItem<String> rootNode = new TreeItem<>("Categories");

  public static void main(String[] args) {
    Application.launch(args);
  }

  @Override
  public void start(Stage stage) {
    VBox vBox = new VBox();
    final Scene scene = new Scene(vBox, 400, 300);

    for (Employee e : employees) {
      TreeItem<String> treeItem = new TreeItem<>();
      treeItem.setValue(e.toString());
      rootNode.getChildren().add(treeItem);
    }

    TreeView<String> treeView = new TreeView<String>(rootNode);
    treeView.setEditable(true);
    treeView.setCellFactory(new Callback<TreeView<String>, TreeCell<String>>()
    {
      @Override
      public TreeCell<String> call(TreeView<String> p) {
        return new TextFieldTreeCellImpl();
      }
    });

    vBox.getChildren().add(treeView);
    stage.setScene(scene);
    stage.show();
  }

  private final class TextFieldTreeCellImpl extends TreeCell<String>
  {
    private TextField textField;
//    private ContextMenu addMenu = new ContextMenu();

    public TextFieldTreeCellImpl() {
      super();
//      MenuItem addMenuItem = new MenuItem("Add Employee");
//      addMenu.getItems().add(addMenuItem);
//      addMenuItem.setOnAction(new EventHandler()
//      {
//        public void handle(Event t) {
//          TreeItem newEmployee =
//              new TreeItem<String>("New Employee");
//          getTreeItem().getChildren().add(newEmployee);
//        }
//      });
    }

    @Override
    public void startEdit() {
      super.startEdit();

      if (textField == null) {
        createTextField();
      }
      setText(null);
      setGraphic(textField);
      textField.selectAll();
    }

    @Override
    public void cancelEdit() {
      System.out.println("cancelEDIT!!!!!!");
      super.cancelEdit();
      setText(textField.getText());
      setGraphic(getTreeItem().getGraphic());
    }

    @Override
    public void updateItem(String item, boolean empty) {
      super.updateItem(item, empty);
      setText(getItem());

//      if (empty) {
//        setText(null);
//        setGraphic(null);
//      } else {
//        if (isEditing()) {
//          if (textField != null) {
//            textField.setText(getString());
//          }
//          setText(null);
//          setGraphic(textField);
//        } else {
//          setText(getString());
////          setGraphic(getTreeItem().getGraphic());
////          if (!getTreeItem().isLeaf() && getTreeItem().getParent() != null) {
////            setContextMenu(addMenu);
////          }
//        }
//      }
    }

    private void createTextField() {
      textField = new TextField(getString());

//      textField.textProperty().addListener((observable, oldValue, newValue) -> {
//        System.out.println("textfield changed from " + oldValue + " to " + newValue);
//      });

//      textField.focusedProperty().addListener((observable, oldValue, newValue) -> {
//        if (oldValue && !newValue) {
//          commitEdit(textField.getText());
//          System.out.println("Textfield leave focus -> " + oldValue + " -> " + newValue);
//          System.out.println(getString());
//          commitEdit(textField.getText() + "----->");
//        }
//      });


//      textField.setOnKeyReleased(new EventHandler<KeyEvent>()
//      {
//        @Override
//        public void handle(KeyEvent t) {
//          if (t.getCode() == KeyCode.ENTER) {
//            commitEdit(textField.getText());
//          } else if (t.getCode() == KeyCode.ESCAPE) {
//            cancelEdit();
//          }
//        }
//      });

    }

    private String getString() {
      return getItem() == null ? "" : getItem().toString();
    }
  }

  public static class Employee
  {
    private final SimpleStringProperty firstname;
    private final SimpleStringProperty lastname;

    public Employee(String firstname, String lastname) {
      this.firstname = new SimpleStringProperty(firstname);
      this.lastname = new SimpleStringProperty(lastname);
    }

    public String getFirstname() {
      return firstname.get();
    }

    public SimpleStringProperty firstnameProperty() {
      return firstname;
    }

    public void setFirstname(String firstname) {
      this.firstname.set(firstname);
    }

    public String getLastname() {
      return lastname.get();
    }

    public SimpleStringProperty lastnameProperty() {
      return lastname;
    }

    public void setLastname(String lastname) {
      this.lastname.set(lastname);
    }

    @Override
    public String toString() {
      return firstname.get() + ", " + lastname.get();
    }
  }
}